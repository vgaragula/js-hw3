// #### Технические требования:
// - Считать с помощью модального окна браузера два числа.
// - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.
// - Создать функцию, в которую передать два значения и операцию.
// - Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:
// - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

let firstNumber = +prompt('Введите первое число')
let secondNumber = +prompt('Введите второе число')
while (!isValidValue(firstNumber) && !isValidValue(secondNumber)) {
    firstNumber = +prompt('Введите первое число!')
    secondNumber = +prompt('Введите второе число!')
}
const mathOperation = prompt('Введите математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.')
console.log(getCalculation(mathOperation))

function getCalculation(operation) {
    switch (operation) {
        case '/':
            return division(firstNumber, secondNumber)
        case '+':
            return summing(firstNumber, secondNumber)
        case '*':
            return multiplication(firstNumber, secondNumber)
        case '-':
            return subtraction(firstNumber, secondNumber)
    }
}
function isValidValue(value) {
    return !!value
}
function division(a, b) {
    return a / b
}
function summing(a, b) {
    return a + b
}
function multiplication(a, b) {
    return a * b
}
function subtraction(a, b) {
    return a - b
}